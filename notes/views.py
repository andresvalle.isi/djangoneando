from django.shortcuts import render, get_object_or_404
from django.http import HttpResponseRedirect
from django.core.urlresolvers import reverse
from notes.models import Note, NoteSet
from students.models import Student
from courses.models import Course

import logging

logger = logging.getLogger(__name__)


def note_list(request, course_id):
    '''
    Listado de notas para un curso
    '''

    course = get_object_or_404(Course, pk=course_id)

    return render(
        request,
        'notes/list.html', {
            'notes': Note.objects.select_related(NoteSet).filter(note_set_course__id=course),
            'course_id': course_id
        }
    )


def note_new(request, course_id):
    '''
    Carga de notas para un curso
    '''
    course = get_object_or_404(Course, pk=course_id)
    students = Student.objects.all()

    return render(
        request,
        'notes/new.html', {
            'course': course,
            'students': students,
            'valid_note_values': range(1, 10),
        }
    )


def note_create(request):
    '''
    Guardado de notas para un curso
    '''
    course_id = request.POST.get('course_id')
    course = get_object_or_404(Course, pk=course_id)
    students = Student.objects.all()
    logger.debug(students)

    new_notes = []
    logger.debug('asd')
    for s in students:
        note_value = request.POST.get('note_' + str(s.id))
        if note_value:
            note = Note( note = note_value, student = s )
            logger.debug(note)
            new_notes.append(note)
            # note.save()

    ns = NoteSet(
        proffesor_id = 1,
        notes = new_notes,
        course = course,
    )
    logger.debug(ns)

    # ns.save()

    return HttpResponseRedirect(reverse('notes:list', args=(course_id,)))

    return render(
        request,
        'notes/new.html', {
            'course': course,
            'students': students,
            'valid_note_values': range(1, 10),
        }
    )
