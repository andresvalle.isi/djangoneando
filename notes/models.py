from __future__ import unicode_literals

from django.db import models
from courses.models import Course
from professors.models import Professor
from students.models import Student

# Create your models here.

class NoteSet(models.Model):
    course = models.OneToOneField(Course)
    professor = models.OneToOneField(Professor)

    def __str__(self):
        return 'Notas by ' + self.professor

class Note(models.Model):
    note = models.IntegerField()
    note_set = models.ForeignKey(NoteSet, on_delete=models.CASCADE)
    student = models.OneToOneField(Student)

    def __str__(self):
        return self.student + ': ' + self.note
