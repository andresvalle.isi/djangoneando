from django.conf.urls import url

from . import views

app_name = 'notes'

urlpatterns = [
    url(r'^(?P<course_id>[0-9]+)/$', views.note_list, name='list'),
    url(r'^(?P<course_id>[0-9]+)/new/$', views.note_new, name='new'),
    url(r'^create/$', views.note_create, name='note_create'),
]
