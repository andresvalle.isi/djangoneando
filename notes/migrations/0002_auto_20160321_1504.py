# -*- coding: utf-8 -*-
# Generated by Django 1.9.2 on 2016-03-21 15:04
from __future__ import unicode_literals

from django.db import migrations, models
import django.db.models.deletion


class Migration(migrations.Migration):

    dependencies = [
        ('professors', '0001_initial'),
        ('courses', '0001_initial'),
        ('notes', '0001_initial'),
    ]

    operations = [
        migrations.CreateModel(
            name='NoteSet',
            fields=[
                ('id', models.AutoField(auto_created=True, primary_key=True, serialize=False, verbose_name='ID')),
                ('course', models.OneToOneField(on_delete=django.db.models.deletion.CASCADE, to='courses.Course')),
                ('professor', models.OneToOneField(on_delete=django.db.models.deletion.CASCADE, to='professors.Professor')),
            ],
        ),
        migrations.RemoveField(
            model_name='note',
            name='course',
        ),
        migrations.RemoveField(
            model_name='note',
            name='professor',
        ),
        migrations.AddField(
            model_name='note',
            name='note_set',
            field=models.ForeignKey(on_delete=django.db.models.deletion.CASCADE, to='notes.NoteSet'),
            preserve_default=False,
        ),
    ]
