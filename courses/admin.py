from django.contrib import admin
from courses.models import Course


class CourseAdmin(admin.ModelAdmin):

    list_display = ('subject', 'year')

    search_fields = ('subject', 'year')

admin.site.register(Course, CourseAdmin)
