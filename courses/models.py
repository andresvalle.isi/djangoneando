from __future__ import unicode_literals

from django.db import models

# Create your models here.


class Course(models.Model):
    subject = models.CharField(max_length=100)
    year = models.IntegerField()

    def label(self):
        return "{} {}".format(self.subject, self.year)

    def __str__(self):
        return self.label()
