from django.shortcuts import render, get_list_or_404
from courses.models import Course
# Create your views here.

import logging

logger = logging.getLogger(__name__)

def course_list(request):
    '''
    Listado de cursos, pantalla incial del frontend
    '''

    courses = get_list_or_404(Course)

    return render(
        request,
        'courses/list.html', {
            'courses': courses
        }
    )
