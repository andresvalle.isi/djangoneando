from django.contrib import admin
from students.models import Student


class StudentAdmin(admin.ModelAdmin):

    list_display = ('dni', 'name', 'lastname')

    search_fields = ('name', 'lastname')

admin.site.register(Student, StudentAdmin)
