from django.contrib import admin
from professors.models import Professor


class ProfessorAdmin(admin.ModelAdmin):

    list_display = ('dni', 'name', 'lastname')

    search_fields = ('name', 'lastname')

admin.site.register(Professor, ProfessorAdmin)
