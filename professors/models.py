from __future__ import unicode_literals

from django.db import models
from courses.models import Course
# Create your models here.


class Professor(models.Model):
    dni = models.IntegerField()
    name = models.CharField(max_length=100)
    lastname = models.CharField(max_length=100)
    courses = models.ManyToManyField(Course)

    def __str__(self):
        return "{}, {}".format(self.lastname, self.name)
